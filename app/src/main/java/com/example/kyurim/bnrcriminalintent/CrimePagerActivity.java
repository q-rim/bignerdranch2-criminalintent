package com.example.kyurim.bnrcriminalintent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

// ViewPager
// used for swiping left and right - Tinder!
public class CrimePagerActivity extends FragmentActivity{
   private ViewPager mViewPager;
   private ArrayList<Crime> mCrimes;

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      mViewPager = new ViewPager(this);
      mViewPager.setId(R.id.viewPager);
      mViewPager.setOffscreenPageLimit(3);   // limits the number of pages pre-loaded to x
      setContentView(mViewPager);

      //mCrimes = CrimeLab.get(this).getCrimes();
      mCrimes = CrimeLab.get(this).getmCrimes();

      FragmentManager fm = getSupportFragmentManager();
      mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {

         @Override
         public int getCount() {
            return mCrimes.size();
         }

         @Override
         public Fragment getItem(int pos) {
            Crime crime = mCrimes.get(pos);
            return CrimeFragment.newInstance(crime.getId());
         }
      });

      // OnPageChangeListener is how you listen for changes in the page currently being displayed by ViewPager.
      //mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {      // setOnPageListener is deprecated
      mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
         public void onPageScrollStateChanged(int state) {
         }

         public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {
         }

         public void onPageSelected(int pos) {
            Crime crime = mCrimes.get(pos);
            if (crime.getTitle() != null) {
               setTitle(crime.getTitle());
            }
         }
      });

      // allows other parts of the list to be selected.
      // Without this, by default, only the first selection is always selected.
      UUID crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);
      for (int i=0; i<mCrimes.size(); i++) {
         if (mCrimes.get(i).getId().equals(crimeId)) {
            mViewPager.setCurrentItem(i);
            break;
         }
      }
   }
}
